package test.java.com.example;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class TestRepeatTalker {
    @Test
    public void testRepeatTalker() {
        RepeatTalker repeatTalker = new RepeatTalker();
        String result = repeatTalker.talk("Hello");

        assertEquals("HelloHello", result);
    }
}